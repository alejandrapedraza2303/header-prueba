import React, {useState} from 'react';
import {AiOutlineClose, AiOutlineMenu} from 'react-icons/ai'
import {FaSearch} from 'react-icons/fa'
import Logo from "../assets/logo_makemake.png"

const Navbar = () => {
    const [nav, setNav] = useState(false)

    const handleNav = () => {
        setNav(!nav)
    }
  return (
    <nav className = "bg-[#dedede]">
    <div className= 'flex items-center font-medium justify-around'>
        <div className= 'flex items-center font-medium justify-around'>
            <div onClick = {handleNav} className='block'>
                {!nav ? <AiOutlineMenu/> : <AiOutlineClose/>}
            </div>
            <h1 className='font-bold'>Explorar</h1>
        </div>
        <div className= 'flex items-center font-medium justify-between'>
        <div className="hidden lg:flex border border-gray rounded">
            <input
                type="text"
                className="block w-full px-40 py-2 bg-white border rounded-md focus:outline-none"
                placeholder="Búsqueda por título, autor"
            />
            </div>
            <div className ='hidden lg:flex'>
                <FaSearch/>
            </div>
        </div>
        <div>
            <img src={Logo} alt="logo" className="md:cursor-pointer" class="object-scale-down h-12" />
        </div>
        <ul className ='hidden xl:flex items-center gap-8 font-medium'>
            <li className='p-4'>¿Qué es?</li>
            <li className='p-4'>Quiero MakeMake</li>
            <li className='p-4'>Blog</li>
            <li className='p-4'>Iniciar Sesión</li>
        </ul>
        <div className = {nav ? ' bg-[#dedede] fixed  left-0 top-0 xl:w-[40%] md:w-[60%] sm:w-[80%]   h-full ease-in-out duration-500' : 'ease-in-out duration-500 fixed left-[-100%] '}>
        <div className= '  bg-white flex items-center font-medium justify-around'>
        <h1 className='e font-bold p-4'>Explorar</h1>
        <div onClick = {handleNav} className='block'>
            {!nav ? <AiOutlineMenu/> :  <AiOutlineClose/>}
        </div>
        </div>
        <div className= 'flex items-center font-medium justify-between'>
        <div className="xl:hidden border border-gray rounded p-4">
            <input
                type="text"
                className="block w-full md:px-10 xl:px-20 py-2 bg-white border rounded-md focus:outline-none"
                placeholder="Busqueda por titulo, autor, editorial"
                autocomplete="off"
            />
            </div>
            <div className ='xl:hidden gap-8'>
                <FaSearch/>
            </div>
        </div>
            <ul className='xl:hidden p-4 font-medium'>
                <li className='p-4'>¿Qué es?</li>
                <li className='p-4'>Quiero MakeMake</li>
                <li className='p-4'>Blog</li>
                <li className='p-4'>Iniciar Sesión</li>
            </ul>
        </div>
    </div>
    </nav>
  );
};

export default Navbar;